// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePath = '/home/rama/ipl/src/data/matches.csv';
fs.readFile(csvFilePath, 'utf8', (err, data) => {
  if (err) {
    console.error('Error reading the CSV file:', err);
    return;
  }

  // Parse the CSV data
  const parsedData = Papa.parse(data, {
    header: true, // Treat the first row as headers
  });

  const matchesData = parsedData.data;

  function highestNoOfPlayerOfTheMatch(matchesData) {
    const distinctYears = matchesData.map((match) => match['season']).filter((year) => year !== undefined).filter((year, index, years) => years.indexOf(year) === index);
    
    const highestManOfTheMatch = distinctYears.reduce((accumulator, season) => {
      const manOfTheMatch = matchesData.reduce((result, match) => {
        if (match['season'] === season) {
          const playerOfMatch = match['player_of_match'];
          result[playerOfMatch] = (result[playerOfMatch] || 0) + 1;
        }
        return result
      }, {});
      const sortedData = Object.fromEntries(
        Object.entries(manOfTheMatch).sort((a, b) => b[1] - a[1])

      );

      accumulator[season] = Object.keys(sortedData)[0];
      return accumulator
    }, {});

    return highestManOfTheMatch;
  }

  let result = highestNoOfPlayerOfTheMatch(matchesData);


  
  // Convert to JSON
  const jsonData = JSON.stringify(result);

  // Write the JSON data to a new file
  fs.writeFile('/home/rama/ipl/src/public/output/6-highest-player-of-the-match.json', jsonData, 'utf8', (err) => {
    if (err) {
      console.error('Error writing JSON data to file:', err);
      return;
    }
    console.log('CSV data converted to JSON and saved as output.json');
  });
});
