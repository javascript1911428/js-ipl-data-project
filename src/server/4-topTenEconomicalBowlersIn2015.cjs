// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePathMatches = '/home/rama/ipl/src/data/matches.csv';
const csvFilePathDeliveries = '/home/rama/ipl/src/data/deliveries.csv';

fs.readFile(csvFilePathMatches, 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }

    // Parse the CSV data
    const parsedDataMatches = Papa.parse(data, {
        header: true, // Treat the first row as headers
    });

    fs.readFile(csvFilePathDeliveries, 'utf8', (err, data) => {
        if (err) {
            console.error('Error reading the CSV file:', err);
            return;
        }
    
        // Parse the CSV data
        const parsedDataDeliveries = Papa.parse(data, {
            header: true, // Treat the first row as headers
        });
    
        let matchesData = parsedDataMatches.data;

        let deliveriesData = parsedDataDeliveries.data;

        function topEconomyBowlersIn2015(matchesData, deliveriesData) {
            const matchesIn2015 = matchesData.filter((match) => match['season'] === "2015");
            //console.log(matchesIn2016);
            const idList = matchesIn2015.map((match) => match['id']).sort()
            //console.log(idList)
            const minimumId = idList[0];
            //console.log(minimumId);
            const maximumId = idList[idList.length - 1];
            //console.log(maximumId);

            const bowlersData = deliveriesData.reduce((accumulator, delivery) => {
                const matchId = parseInt(delivery['match_id']);
                //console.log(typeof(matchId));

                if(matchId >= parseInt(minimumId)  && matchId <= parseInt(maximumId)) {
                    const bowler = (delivery['bowler']);
                    //console.log(bowlingTeam);
                    // const extraRuns = parseInt(delivery['extra_runs']);
                    // if(!accumulator[bowlingTeam]) {
                    //     accumulator[bowlingTeam] = extraRuns;
                    // } else {
                    //     accumulator[bowlingTeam] += extraRuns;
                    // }
                    const totalRuns = delivery['total_runs'];
                    //console.log(typeof(totalRuns));
                    const extraRuns = delivery['extra_runs'];
                    const penaltyRuns = delivery['penalty_runs'];
                    const byeRuns = delivery['bye_runs'];
                    const legByeRuns = delivery['legbye_runs'];
                    const wideRuns = delivery['wide_runs']
                    const noballRuns = delivery['noball_runs'];

                    if (!accumulator[bowler]) {
                        accumulator[bowler] = {runs: 0, balls: 0};
                    }

                    if(wideRuns === "0" && noballRuns === "0") {
                        accumulator[bowler]['runs'] += (parseInt(totalRuns) - parseInt(extraRuns));
                        accumulator[bowler]['balls']++;
                    }
                }

                    return accumulator
                }, {});
            
            for (const bowler in bowlersData) {
                const runs = bowlersData[bowler]['runs']
                const balls = bowlersData[bowler]['balls']
                bowlersData[bowler].economy = (runs / balls) * 6;
            }

            const sortedBowlers = Object.entries(bowlersData).sort(
                (a, b) => a[1].economy - b[1].economy
            );

            const top10EconomyBowlers = sortedBowlers.slice(0, 10).map((entry) => ({
                bowler: entry[0],
                economy: entry[1].economy,
            }));

            return top10EconomyBowlers;
        }

        let result = topEconomyBowlersIn2015(matchesData, deliveriesData)
        
    // Convert to JSON
        const jsonData = JSON.stringify(result);

    // Write the JSON data to a new file
    fs.writeFile('/home/rama/ipl/src/public/output/4-top-ten-economical-bowlers.json', jsonData, 'utf8', (err) => {
        if (err) {
        console.error('Error writing JSON data to file:', err);
        return;
        }
        console.log('CSV data converted to JSON and saved as output.json');
    });

});
});
