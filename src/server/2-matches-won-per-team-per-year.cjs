// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePath = '/home/rama/ipl/src/data/matches.csv';
fs.readFile(csvFilePath, 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }

    // Parse the CSV data
    const parsedData = Papa.parse(data, {
        header: true, // Treat the first row as header console.log(parsedData.length);
    });

    //let matchesWonPerTeamPerYear = {};

    // distinct years

    // let distinctYears = []

    // for (let index = 0; index < parsedData.data.length; index++) {
    //     let year = parsedData.data[index]['season'];
    //     if(year != undefined) {
    //         if(!distinctYears.includes(year)) {
    //             distinctYears.push(year)
    //         }
    //     }
    // }

    // console.log(distinctYears);

    // for(let year of distinctYears) {
    //     let matchesWon = {};
    //     for (index = 0; index < parsedData.data.length; index++) {
    //         if(parsedData.data[index]['season'] === year) {
    //             if (matchesWon[parsedData.data[index]['winner']] === undefined) {
    //                 matchesWon[parsedData.data[index]['winner']] = 1
    //             } else {
    //                 matchesWon[parsedData.data[index]['winner']] += 1
    //             }
    //         }
    //     }

    //     matchesWonPerTeamPerYear[year] = matchesWon;
    // }

    let iplMatchesInfo = parsedData.data;

    function getMatchesWonPerTeamPerYear(matches) {
        const matchesWonPerTeamPerYear = matches.reduce((accumulator, match) => {
            const season = match['season'];
            const winner = match['winner'];

            if(!accumulator[season]) {
                accumulator[season] = {};
            }

            if(!accumulator[season][winner]) {
                accumulator[season][winner] = 1;
            } else {
                accumulator[season][winner] += 1;
            }

            return accumulator;

        }, {})

        return matchesWonPerTeamPerYear;
    }

    let result = getMatchesWonPerTeamPerYear(iplMatchesInfo);
    
    // Convert to JSON
    const jsonData = JSON.stringify(result);

    // Write the JSON data to a new file
    fs.writeFile('/home/rama/ipl/src/public/output/2-matches-won-per-team-per-year.json', jsonData, 'utf8', (err) => {
        if (err) {
            console.error('Error writing JSON data to file:', err);
            return;
        }
        console.log('CSV data converted to JSON and saved as output.json');
    });
});
