// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePathMatches = '/home/rama/ipl/src/data/matches.csv';
const csvFilePathDeliveries = '/home/rama/ipl/src/data/deliveries.csv';

fs.readFile(csvFilePathMatches, 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }

    // Parse the CSV data
    const parsedDataMatches = Papa.parse(data, {
        header: true, // Treat the first row as headers
    });

    fs.readFile(csvFilePathDeliveries, 'utf8', (err, data) => {
        if (err) {
            console.error('Error reading the CSV file:', err);
            return;
        }
    
        // Parse the CSV data
        const parsedDataDeliveries = Papa.parse(data, {
            header: true, // Treat the first row as headers
        });

    let matchesData = parsedDataMatches.data;
    let deliveriesData = parsedDataDeliveries.data;

    function strikerateOfBatsmenEverySeason(matchesData, deliveriesData) {
        const distinctYears = matchesData.map((match) => match['season']).filter((year) => year !== undefined).filter((year, index, years) => years.indexOf(year) === index);
        const matchIdForSeason = distinctYears.reduce((accumulator, year) => {
            const yearList = matchesData.filter((match) => match['season'] === year).map((match) => match['id']);
            accumulator[year] = yearList;
            return accumulator
        }, {})

        const toatlRunsByBatsmanSeason = distinctYears.reduce((accumulator, year) => {
            accumulator[year] = deliveriesData.reduce((result, delivery) => {
                if(matchIdForSeason[year].includes(delivery['match_id'])) {
                    const batsman = delivery['batsman'];
                    const runs = parseInt(delivery['batsman_runs']);
                    const wideRuns = delivery['wide_runs'];
                    const noballRuns = delivery['noball_runs'];

                    if(!result[batsman]) {
                        result[batsman] = {runs:0, balls:0};
                    }

                    result[batsman].runs += runs;

                    if (wideRuns === '0' && noballRuns === '0') {
                        result[batsman].balls++;
                    }
                }
                return result;
            }, {});
            return accumulator;
        }, {});

        const strikeRateByBatsmanSeason = distinctYears.reduce((accumulator, year) => {
            accumulator[year] = {};
            Object.entries(toatlRunsByBatsmanSeason[year]).map(([batsman, stats]) => {
                const {runs, balls} = stats;
                accumulator[year][batsman] = ((runs / balls) * 100).toFixed(2);
            });
            return accumulator
        }, {});

        return strikeRateByBatsmanSeason;
    }

    let result = strikerateOfBatsmenEverySeason(matchesData, deliveriesData);
    

    
        
    const jsonData = JSON.stringify(result);

    // Convert to JSON
    //const jsonData = JSON.stringify(parsedData.data);

    // Write the JSON data to a new file
    fs.writeFile('/home/rama/ipl/src/public/output/7-strikerate-of-batsmen-each-season.json', jsonData, 'utf8', (err) => {
        if (err) {
        console.error('Error writing JSON data to file:', err);
        return;
        }
        console.log('CSV data converted to JSON and saved as output.json');
    });

});
});
