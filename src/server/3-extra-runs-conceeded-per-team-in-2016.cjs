// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePathMatches = '/home/rama/ipl/src/data/matches.csv';

const csvFilePathDeliveries = '/home/rama/ipl/src/data/deliveries.csv';


fs.readFile(csvFilePathMatches, 'utf8', (err, data) => {

    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }

    // Parse the CSV data
    const parsedDataMatches = Papa.parse(data, {
        header: true, // Treat the first row as headers
    });


    fs.readFile(csvFilePathDeliveries, 'utf8', (err, data1) => {

        if (err) {
            console.error('Error reading the CSV file:', err);
            return;
        }
    
        // Parse the CSV data
        const parsedDataDeliveries = Papa.parse(data1, {
            header: true, // Treat the first row as headers
        });

        let matchesData = parsedDataMatches.data;

        let deliveriesData = parsedDataDeliveries.data;

        function extraRunsConcededPerTeamIn2016(matchesData, deliveriesData) {
            const matchesIn2016 = matchesData.filter((match) => match['season'] === "2016");
            //console.log(matchesIn2016);
            const idList = matchesIn2016.map((match) => match['id']).sort()
            //console.log(idList)
            const minimumId = idList[0];
            //console.log(minimumId);
            const maximumId = idList[idList.length - 1];
            //console.log(maximumId);

            const extraRunsConcededPerTeam = deliveriesData.reduce((accumulator, delivery) => {
                const matchId = parseInt(delivery['match_id']);
                //console.log(typeof(matchId));

                if(matchId >= parseInt(minimumId)  && matchId <= parseInt(maximumId)) {
                    const bowlingTeam = (delivery['bowling_team']);
                    console.log(bowlingTeam);
                    const extraRuns = parseInt(delivery['extra_runs']);
                    if(!accumulator[bowlingTeam]) {
                        accumulator[bowlingTeam] = extraRuns;
                    } else {
                        accumulator[bowlingTeam] += extraRuns;
                    }
                }
                return accumulator;
            }, {})
            return extraRunsConcededPerTeam
        }

        let result = extraRunsConcededPerTeamIn2016(matchesData, deliveriesData)
        

        
    
        const jsonData = JSON.stringify(result);

  

  // Write the JSON data to a new file
  fs.writeFile('/home/rama/ipl/src/public/output/3-extra-runs-conceeded-per-year-team-in-2016.json', jsonData, 'utf8', (err) => {

    if (err) {
      console.error('Error writing JSON data to file:', err);
      return;
    }
    console.log('CSV data converted to JSON and saved as output.json');
  });
})});
