// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePath = '/home/rama/ipl/src/data/matches.csv';
fs.readFile(csvFilePath, 'utf8', (err, data) => {
    if (err) {
      console.error('Error reading the CSV file:', err);
      return;
    }

    // Parse the CSV data
    const parsedData = Papa.parse(data, {
      header: true, // Treat the first row as headers
    });


  
    const matchesOfAllYears = parsedData.data;

    //let matchesPlayesPerYear = {};

    function getMatchesPerYear(matchesOfAllYears) {
      const matchesPerYear = matchesOfAllYears.reduce((accumulator, match) => {
        let season = match['season'];
        if(accumulator[season] === undefined && season != null) {
          accumulator[season] = 1;
        } else {
          accumulator[season] += 1;
        }
        return accumulator
      }, {})
      return matchesPerYear;
    }

    let result = getMatchesPerYear(matchesOfAllYears);

      
    const jsonData = JSON.stringify(result);

    // Convert to JSON
    //const jsonData = JSON.stringify(parsedData.data);

    // Write the JSON data to a new file
    fs.writeFile('/home/rama/ipl/src/public/output/1-matches-per-year.json', jsonData, 'utf8', (err) => {
      if (err) {
        console.error('Error writing JSON data to file:', err);
        return;
      }
      console.log('CSV data converted to JSON and saved as output.json');
    });
  });
