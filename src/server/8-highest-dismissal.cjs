// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePath = '/home/rama/ipl/src/data/deliveries.csv';
fs.readFile(csvFilePath, 'utf8', (err, data) => {
    if (err) {
      console.error('Error reading the CSV file:', err);
      return;
    }

    // Parse the CSV data
    const parsedData = Papa.parse(data, {
      header: true, // Treat the first row as headers
    });

    // let dismissalsData = {}

    // for (index = 0; index < parsedData.data.length; index++) {
    //   if(parsedData.data[index]['batsman'] !== undefined && parsedData.data[index]['dismissal_kind'] !== "run_out") {
    //     dismissalsData[parsedData.data[index]['batsman']] = parsedData.data[index]['bowler']; 
    //   }
    // }

    let deliveriesData = parsedData.data;

    function findHighestDismissals(deliveriesData) {
      const dismissalCount = deliveriesData.reduce((accumulator, dismissal) => {
        const batsman = dismissal['player_dismissed'];
        const bowler = dismissal['bowler'];
        const dismissalKind = dismissal['dismissal_kind']

        if (dismissalKind !== "run out" && batsman !== "") {
          if (!accumulator[batsman]) {
            accumulator[batsman] = {};
          }

          if (!accumulator[batsman][bowler]) {
            accumulator[batsman][bowler] = 1;
          } else {
            accumulator[batsman][bowler]++;
          }
        }
        return accumulator
      }, {});

      const highestDismissals = Object.entries(dismissalCount).reduce((accumulator, [batsman, bowlerStats]) => {
        const bowlerWithMaxDismissals = Object.entries(bowlerStats).reduce((maxDismissals, [bowler, count]) => {
          if (count > maxDismissals.count) {
            return {bowler, count};
          } else {
            return maxDismissals
          }
        }, {bowler: '', count: 0});
        accumulator[batsman] = { bowler: bowlerWithMaxDismissals.bowler, count: bowlerWithMaxDismissals.count};
        return accumulator
      }, {});
      
      const sortedDismissals = Object.entries(highestDismissals).sort((a, b) => b[1].count - a[1].count);
      return sortedDismissals[0];
    }

    let result = findHighestDismissals(deliveriesData)

    
    
    
    
    
    
    const jsonData = JSON.stringify(result);

    // Convert to JSON
    //const jsonData = JSON.stringify(parsedData.data);

    // Write the JSON data to a new file
    fs.writeFile('/home/rama/ipl/src/public/output/8-highest-dismissal.json', jsonData, 'utf8', (err) => {
      if (err) {
        console.error('Error writing JSON data to file:', err);
        return;
      }
      console.log('CSV data converted to JSON and saved as output.json');
    });
});
