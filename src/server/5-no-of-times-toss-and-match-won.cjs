// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePath = '/home/rama/ipl/src/data/matches.csv';
fs.readFile(csvFilePath, 'utf8', (err, data) => {
    if (err) {
      console.error('Error reading the CSV file:', err);
      return;
    }

    // Parse the CSV data
    const parsedData = Papa.parse(data, {
      header: true, // Treat the first row as headers
    });



    let matchesData = parsedData.data;

    const matchEntries = matchesData.filter((entry) => entry['winner'] === entry['toss_winner']);

    const noOfTimesMatchAndTossWon = matchEntries.reduce((accumulator, entry) => {
        if(!accumulator[entry['toss_winner']]) {
          accumulator[entry['toss_winner']] = 1;
        
        } else {
          accumulator[entry['toss_winner']] += 1;
        }
        return accumulator;
    }, {})


    // Convert to JSON
    const jsonData = JSON.stringify(noOfTimesMatchAndTossWon);

    // Write the JSON data to a new file
    fs.writeFile('/home/rama/ipl/src/public/output/5-no-of-times-toss-and-match-won.json', jsonData, 'utf8', (err) => {
      if (err) {
        console.error('Error writing JSON data to file:', err);
        return;
      }
      console.log('CSV data converted to JSON and saved as output.json');
    });
});
